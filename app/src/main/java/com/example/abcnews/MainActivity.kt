package com.example.abcnews
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.OrientationHelper
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.example.abcnews.model.Feed
import com.example.abcnews.model.Item
import com.example.abcnews.model.articles
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {
    private val dataList: MutableList<Item> = mutableListOf()
    private val dataImage: MutableList<Feed> = mutableListOf()
    private lateinit var adapter: MyAdapter
    //private lateinit var images: MyAdapter.ImageClass


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        title = "Title"
        //setup adapter
        adapter = MyAdapter(dataList)
        //images= MyAdapter.ImageClass(dataImage)

        my_recycler.layoutManager = LinearLayoutManager(this)
        my_recycler.addItemDecoration(DividerItemDecoration(this, OrientationHelper.VERTICAL))
        my_recycler.adapter = adapter


        AndroidNetworking.initialize(this)
        AndroidNetworking.get("https://api.rss2json.com/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml")
            .build()
            .getAsObject(articles::class.java, object : ParsedRequestListener<articles> {
                override fun onResponse(response: articles) {
                    dataList.addAll(response.items)
                    //dataImage.addAll(response.feed)
                    //images.notifyDataSetChanged()
                    adapter.notifyDataSetChanged()
                }

                override fun onError(anError: ANError?) {

                }
            })




    }
}

