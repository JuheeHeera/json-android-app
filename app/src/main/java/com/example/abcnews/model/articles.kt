package com.example.abcnews.model
import com.google.gson.annotations.SerializedName

data class articles(
    @SerializedName("feed")
    val feed: Feed,
    @SerializedName("items")
    val items: List<Item>,
    @SerializedName("status")
    val status: String
)