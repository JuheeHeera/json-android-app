package com.example.abcnews

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.abcnews.model.Feed
import com.example.abcnews.model.Item
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_list_layout.view.*


class MyAdapter (private val dataList: MutableList<Item>) : RecyclerView.Adapter<MyHolder>() {

        private lateinit var context: Context


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
            //To change body of created functions use File | Settings | File Templates.
            context = parent.context
            return MyHolder(
                LayoutInflater.from(context).inflate(R.layout.custom_list_layout, parent, false)
            )
        }

        override fun getItemCount(): Int = dataList.size
        //{
        //To change body of created functions use File | Settings | File Templates.

        //}

        override fun onBindViewHolder(holder: MyHolder, position: Int) {
            //To change body of created functions use File | Settings | File Templates.
            val data = dataList[position]

            val articleTitleTextView = holder.itemView.articleTitle
            val publishDateTextView = holder.itemView.publishDate
            val title = "${data.title}"
            articleTitleTextView.text = title
            val publishDate = "${data.pubDate}"
            publishDateTextView.text = publishDate
            val image= holder.itemView.Image
            Picasso.get()
                .load(data.enclosure.link)
                .into(image)
            /*Picasso.get()
                .load(data.enclosure)
                .into(image)*/

            holder.itemView.setOnClickListener {
                Toast.makeText(context, title, Toast.LENGTH_SHORT).show()
            }

        }


    }
    /*class ImageClass(private val dataImage: MutableList<Feed>) : RecyclerView.Adapter<MyHolder>() {

        private  lateinit var context: Context



        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
            //To change body of created functions use File | Settings | File Templates.
            context = parent.context
            return MyHolder(LayoutInflater.from(context).inflate(R.layout.custom_list_layout, parent, false))
        }

        override fun getItemCount(): Int = dataImage.size
        //{
        //To change body of created functions use File | Settings | File Templates.

        //}

        override fun onBindViewHolder(holder: MyHolder, position: Int) {
            //To change body of created functions use File | Settings | File Templates.
            val data= dataImage[position]
            val image= holder.itemView.Image
            //Picasso.get()
               // .load(data.image)
                //.into(image)

        }

    }*/



